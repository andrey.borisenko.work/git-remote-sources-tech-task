import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer()).get('/').expect(200).expect('OK');
  });

  it(`should deny non 'application/json' accept header`, () => {
    return request(app.getHttpServer())
      .get('/')
      .set('Accept', 'application/xml')
      .expect(406);
  });

  it(`should allow accept header with 'application/json' and other content types`, () => {
    return request(app.getHttpServer())
      .get('/')
      .set('Accept', 'application/xml,application/json')
      .expect(200);
  });
});
