import { INestApplication } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { LoggerModule } from 'nestjs-pino';
import * as request from 'supertest';

import { AcceptHeaderGuard } from '@api/guards';
import { RemoteSourcesApiModule } from '@api/modules';
import { gitRepositoriesArrayAssertion } from '@app/common/test';

describe('RemoteSourcesApiController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [LoggerModule.forRoot(), RemoteSourcesApiModule],
      providers: [
        {
          provide: APP_GUARD,
          useClass: AcceptHeaderGuard,
        },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  describe('/remote-sources/:source/users/:user/repositories (GET)', () => {
    describe('source = gitlab', () => {
      it('should return user own repositories', async () => {
        const response = await request(app.getHttpServer())
          .get('/remote-sources/github/users/test/repositories')
          .set('Accept', 'application/json')
          .expect(200);

        gitRepositoriesArrayAssertion(response.body);
      });

      it('should return 404 error', async () => {
        return request(app.getHttpServer())
          .get(
            '/remote-sources/github/users/14e4d677-6923-4551-b5c2-f94cc9fce819/repositories',
          )
          .set('Accept', 'application/json')
          .expect(404);
      });
    });
  });
});
