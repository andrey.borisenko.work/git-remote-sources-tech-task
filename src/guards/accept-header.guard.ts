import {
  CanActivate,
  ExecutionContext,
  NotAcceptableException,
} from '@nestjs/common';
import { Request } from 'express';

export class AcceptHeaderGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean {
    const request: Request = context.switchToHttp().getRequest();
    const acceptHeader = request.headers['accept'];

    if (acceptHeader && !acceptHeader.includes('application/json')) {
      throw new NotAcceptableException(
        'Only "application/json" response content type is supported',
      );
    }

    return true;
  }
}
