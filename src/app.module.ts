import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { LoggerModule } from 'nestjs-pino';

import { AcceptHeaderGuard } from './guards';
import { appConfig } from './app.config';
import { AppController } from './app.controller';
import { RemoteSourcesApiModule } from './modules';
import { AllExceptionsFilter } from './exception-filters/all.exception-filter';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ConfigModule.forFeature(appConfig),
    LoggerModule.forRoot(),
    RemoteSourcesApiModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AcceptHeaderGuard,
    },
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
  ],
})
export class AppModule {}
