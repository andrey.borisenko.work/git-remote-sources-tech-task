import { BadRequestException, Param, ParseEnumPipe } from '@nestjs/common';

import { GitRemoteSources } from '@app/git-remote-sources';

export const GitRemoteSourceParam = (param = 'source') =>
  Param(
    param,
    new ParseEnumPipe(GitRemoteSources, {
      exceptionFactory: () =>
        new BadRequestException(
          `Remote source path param "${param}" should be one of the following: ${Object.values(
            GitRemoteSources,
          )}`,
        ),
    }),
  );
