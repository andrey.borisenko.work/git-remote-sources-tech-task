import { Injectable } from '@nestjs/common';

import { GitRepository } from '@app/domain';
import {
  GitRemoteSources,
  GitRemoteSourcesService,
} from '@app/git-remote-sources';

@Injectable()
export class RemoteSourcesApiService {
  constructor(
    private readonly gitRemoteSourcesService: GitRemoteSourcesService,
  ) {}

  async getUserOwnRepositories(
    source: GitRemoteSources,
    user: string,
  ): Promise<GitRepository[]> {
    return await this.gitRemoteSourcesService.getOwnUserRepositories(
      source,
      user,
    );
  }
}
