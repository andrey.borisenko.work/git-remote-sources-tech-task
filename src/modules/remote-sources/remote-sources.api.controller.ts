import { Controller, Get, Param } from '@nestjs/common';

import { GitRepository } from '@app/domain';
import { GitRemoteSources } from '@app/git-remote-sources';

import { GitRemoteSourceParam } from '@api/decorators';

import { RemoteSourcesApiService } from './remote-sources.api.service';

@Controller('remote-sources')
export class RemoteSourcesApiController {
  constructor(private readonly service: RemoteSourcesApiService) {}

  @Get(':source/users/:user/repositories')
  getUserOwnRepositories(
    @GitRemoteSourceParam() source: GitRemoteSources,
    @Param('user') user: string,
  ): Promise<GitRepository[]> {
    return this.service.getUserOwnRepositories(source, user);
  }
}
