import { deepClone } from '@app/common';
import { gitRepositoriesArrayAssertion } from '@app/common/test';
import {
  GitRemoteSources,
  GitRemoteSourcesModule,
  RateLimitReachedError,
  UserNotFoundError,
} from '@app/git-remote-sources';
import { githubRepositoryBranchesFixture } from '@app/git-remote-sources/sources/github/test/fixtures/github-repository-branches.fixture';
import { githubUserRepositoriesFixture } from '@app/git-remote-sources/sources/github/test/fixtures/github-user-repositories.fixture';
import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { AxiosError, AxiosResponse } from 'axios';
import { LoggerModule } from 'nestjs-pino';
import { of, throwError } from 'rxjs';
import { RemoteSourcesApiController } from '../remote-sources.api.controller';
import { RemoteSourcesApiService } from '../remote-sources.api.service';

describe('Integration:RemoteSources', () => {
  let controller: RemoteSourcesApiController;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [GitRemoteSourcesModule, LoggerModule.forRoot()],
      providers: [RemoteSourcesApiService],
      controllers: [RemoteSourcesApiController],
    }).compile();

    controller = module.get(RemoteSourcesApiController);
    httpService = module.get(HttpService, { strict: false });
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
    expect(httpService).toBeDefined();
  });

  describe('Github', () => {
    it('should return user own repositories', async () => {
      const mocked = jest
        .spyOn(httpService, 'get')
        .mockImplementationOnce(() =>
          of({
            data: deepClone(githubUserRepositoriesFixture),
          } as AxiosResponse),
        )
        .mockImplementation(() =>
          of({
            data: deepClone(githubRepositoryBranchesFixture),
          } as AxiosResponse),
        );

      const repositories = await controller.getUserOwnRepositories(
        GitRemoteSources.GITHUB,
        'test',
      );

      gitRepositoriesArrayAssertion(repositories);

      mocked.mockClear();
    });

    it('should throw UserNotFoundError', () => {
      jest.spyOn(httpService, 'get').mockImplementationOnce(() =>
        throwError(
          () =>
            new AxiosError('', '404', {}, {}, {
              data: {
                message: 'Not Found',
              },
            } as AxiosResponse),
        ),
      );

      expect(
        controller.getUserOwnRepositories(GitRemoteSources.GITHUB, 'test'),
      ).rejects.toThrowError(UserNotFoundError);
    });

    it('should throw RateLimitReachedError', () => {
      jest.spyOn(httpService, 'get').mockImplementationOnce(() =>
        throwError(
          () =>
            new AxiosError('', '403', {}, {}, {
              status: 403,
              data: {
                message: 'API rate limit exceeded',
              },
            } as AxiosResponse),
        ),
      );

      expect(
        controller.getUserOwnRepositories(GitRemoteSources.GITHUB, 'test'),
      ).rejects.toThrowError(RateLimitReachedError);
    });
  });
});
