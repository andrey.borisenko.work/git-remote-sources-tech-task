import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';

import { gitRepositoriesArrayAssertion } from '@app/common/test';
import {
  GitRemoteSources,
  GitRemoteSourcesService,
  UserNotFoundError,
} from '@app/git-remote-sources';
import { gitRemoteSourcesServiceMock } from '@app/git-remote-sources/test/mocks/git-remote-sources.service.mock';

import { RemoteSourcesApiService } from '../remote-sources.api.service';

describe('RemoteSourcesApiService', () => {
  let service: RemoteSourcesApiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RemoteSourcesApiService,
        {
          provide: GitRemoteSourcesService,
          useValue: gitRemoteSourcesServiceMock,
        },
      ],
    }).compile();

    service = module.get(RemoteSourcesApiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return user own repositories', async () => {
    const repositories = await service.getUserOwnRepositories(
      GitRemoteSources.GITHUB,
      'test',
    );

    gitRepositoriesArrayAssertion(repositories);
  });

  it('should throw UserNotFoundError', async () => {
    jest
      .spyOn(gitRemoteSourcesServiceMock, 'getOwnUserRepositories')
      .mockRejectedValueOnce(
        new UserNotFoundError(GitRemoteSources.GITHUB, 'test'),
      );

    expect(
      service.getUserOwnRepositories(GitRemoteSources.GITHUB, 'test'),
    ).rejects.toThrowError(UserNotFoundError);
  });
});
