import { GitRemoteSourcesModule } from '@app/git-remote-sources';
import { Module } from '@nestjs/common';
import { RemoteSourcesApiController } from './remote-sources.api.controller';
import { RemoteSourcesApiService } from './remote-sources.api.service';

@Module({
  imports: [GitRemoteSourcesModule],
  providers: [RemoteSourcesApiService],
  controllers: [RemoteSourcesApiController],
})
export class RemoteSourcesApiModule {}
