import { GitBranch } from './git-branch.interface';

export interface GitRepository {
  name: string;
  owner: string;
  branches: GitBranch[];
}
