export interface GitBranch {
  name: string;
  lastCommitSha: string;
}
