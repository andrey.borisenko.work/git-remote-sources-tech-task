import { Provider, Type } from '@nestjs/common';

import { GitRemoteSources } from './enums';
import { GitRemoteSource } from './interfaces';
import { GithubRemoteSource } from './sources/github';

export type RemoteSourcesImplementations = Readonly<
  Record<GitRemoteSources, GitRemoteSource>
>;

export const REMOTE_SOURCES_IMPLEMENTATIONS =
  'RemoteProvidersImplementationsToken';

export const remoteSourcesImplementationsProvider: Provider<RemoteSourcesImplementations> =
  {
    provide: REMOTE_SOURCES_IMPLEMENTATIONS,
    useFactory: (...implementations: GitRemoteSource[]) => {
      const [github] = implementations;

      return {
        [GitRemoteSources.GITHUB]: github,
      };
    },
    inject: [GithubRemoteSource] as Type<GitRemoteSource>[],
  };
