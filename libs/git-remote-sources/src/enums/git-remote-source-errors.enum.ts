export enum GitRemoteSourceErrors {
  UserNotFound = 'UserNotFound',
  UnimplementedSource = 'UnimplementedSource',
  RateLimitReached = 'RateLimitReached',
}
