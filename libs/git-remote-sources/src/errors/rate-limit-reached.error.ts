import { GitRemoteSourceErrors, GitRemoteSources } from '../enums';
import { BaseGitRemoteSourceError } from './base-git-remote-source.error';

export interface RateLimitReachedErrorDetails {
  quota?: string;
  nextRequestTimestamp?: number;
}

export class RateLimitReachedError extends BaseGitRemoteSourceError {
  readonly type = GitRemoteSourceErrors.RateLimitReached;

  constructor(
    readonly source: GitRemoteSources,
    readonly details?: RateLimitReachedErrorDetails,
  ) {
    super(`API calls rate limit reached for ${source} remote source`);
  }
}
