import { GitRemoteSourceErrors, GitRemoteSources } from '../enums';
import { BaseGitRemoteSourceError } from './base-git-remote-source.error';

export class UserNotFoundError extends BaseGitRemoteSourceError {
  readonly type = GitRemoteSourceErrors.UserNotFound;

  constructor(readonly source: GitRemoteSources, readonly username: string) {
    super(`User ${username} not found in ${source} remote source`);
  }
}
