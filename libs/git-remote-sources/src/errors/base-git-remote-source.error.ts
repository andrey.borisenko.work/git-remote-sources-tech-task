import { GitRemoteSourceErrors, GitRemoteSources } from '../enums';

export abstract class BaseGitRemoteSourceError extends Error {
  abstract readonly type: GitRemoteSourceErrors;
  abstract readonly source: GitRemoteSources;
}
