import { GitRemoteSourceErrors, GitRemoteSources } from '../enums';
import { BaseGitRemoteSourceError } from './base-git-remote-source.error';

export class UnimplementedRemoteSourceError extends BaseGitRemoteSourceError {
  readonly type = GitRemoteSourceErrors.UnimplementedSource;

  constructor(readonly source: GitRemoteSources) {
    super(`Implementation for ${source} remote source not found`);
  }
}
