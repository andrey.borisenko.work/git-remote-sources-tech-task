import { ErrorResponse } from '@app/domain';
import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpStatus,
} from '@nestjs/common';
import { HttpAdapterHost } from '@nestjs/core';

import { GitRemoteSourceErrors } from '../enums';
import { BaseGitRemoteSourceError } from '../errors/base-git-remote-source.error';

@Catch(BaseGitRemoteSourceError)
export class GitRemoteSourcesHttpExceptionsFilter
  implements ExceptionFilter<BaseGitRemoteSourceError>
{
  constructor(private readonly httpAdapterHost: HttpAdapterHost) {}

  private mapError(error: BaseGitRemoteSourceError): ErrorResponse {
    switch (error.type) {
      case GitRemoteSourceErrors.UserNotFound:
        return {
          status: HttpStatus.NOT_FOUND,
          Message: error.message,
        };
      case GitRemoteSourceErrors.RateLimitReached:
        return {
          status: HttpStatus.TOO_MANY_REQUESTS,
          Message: error.message,
        };
      default:
        return {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          Message: 'Internal Server Error',
        };
    }
  }

  catch(exception: BaseGitRemoteSourceError, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();

    const response = this.mapError(exception);

    this.httpAdapterHost?.httpAdapter?.reply(
      ctx.getResponse(),
      response,
      response.status,
    );
  }
}
