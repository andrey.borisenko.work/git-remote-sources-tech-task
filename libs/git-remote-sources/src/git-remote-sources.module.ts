import { Module } from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

import { REMOTE_SOURCE_MODULES } from './sources';
import { GitRemoteSourcesService } from './git-remote-sources.service';
import { remoteSourcesImplementationsProvider } from './remote-sources-implementations.provider';
import { GitRemoteSourcesHttpExceptionsFilter } from './exception-filters/http.exception-filter';

@Module({
  imports: [...REMOTE_SOURCE_MODULES],
  providers: [
    GitRemoteSourcesService,
    remoteSourcesImplementationsProvider,
    {
      provide: APP_FILTER,
      useClass: GitRemoteSourcesHttpExceptionsFilter,
    },
  ],
  exports: [GitRemoteSourcesService],
})
export class GitRemoteSourcesModule {}
