import { GitRepository } from '@app/domain';

export interface GitRemoteSource {
  getUserOwnRepositories(username: string): Promise<GitRepository[]>;
}
