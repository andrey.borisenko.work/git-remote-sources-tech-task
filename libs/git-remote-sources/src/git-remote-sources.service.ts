import { GitRepository } from '@app/domain';
import { Inject, Injectable } from '@nestjs/common';

import { GitRemoteSources } from './enums';
import { UnimplementedRemoteSourceError } from './errors';
import { GitRemoteSource } from './interfaces';
import {
  RemoteSourcesImplementations,
  REMOTE_SOURCES_IMPLEMENTATIONS,
} from './remote-sources-implementations.provider';

@Injectable()
export class GitRemoteSourcesService {
  constructor(
    @Inject(REMOTE_SOURCES_IMPLEMENTATIONS)
    private readonly implementations: RemoteSourcesImplementations,
  ) {}

  getRemoteSource(source: GitRemoteSources): GitRemoteSource {
    const implementation = this.implementations[source];

    if (!implementation) {
      throw new UnimplementedRemoteSourceError(source);
    }

    return implementation;
  }

  /**
   * Returns an array of simplified git repositories by user in searched provider
   * @param source one of the available remote cloud providers
   * @param username username in particular provider
   * @returns array of git repositories
   *
   * @throws {UnknownUserError} if user not found for specified provider
   * @throws {UnimplementedRemoteProviderError} if user not found for specified provider
   */
  async getOwnUserRepositories(
    source: GitRemoteSources,
    username: string,
  ): Promise<GitRepository[]> {
    const remoteSource = this.getRemoteSource(source);

    return await remoteSource.getUserOwnRepositories(username);
  }
}
