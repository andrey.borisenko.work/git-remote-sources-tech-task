import { Type } from '@nestjs/common';

import { GithubModule } from './github';

export const REMOTE_SOURCE_MODULES: Type<any>[] = [GithubModule];
