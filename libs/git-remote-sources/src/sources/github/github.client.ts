import { HttpService } from '@nestjs/axios';
import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { AxiosError, AxiosRequestHeaders } from 'axios';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import {
  catchError,
  EMPTY,
  expand,
  filter,
  from,
  map,
  mergeMap,
  Observable,
  of,
  throwError,
} from 'rxjs';

import { isAxiosError } from '@app/common';
import { GitRemoteSources } from '@app/git-remote-sources/enums';
import {
  RateLimitReachedError,
  UserNotFoundError,
} from '@app/git-remote-sources/errors';

import { githubConfig } from './github.config';
import {
  GithubErrorResponse,
  GithubRepository,
  GithubRepositoryBranch,
} from './github.interfaces';

/**
 * Detects user not found error
 * @param {AxiosError} error
 */
const isUserNotFoundError = (error: AxiosError<GithubErrorResponse>) =>
  error.response?.data?.message.toLowerCase().includes('not found');

/**
 * Detects rate limit error
 * @param {AxiosError} error
 */
const isRateLimitError = (error: AxiosError<GithubErrorResponse>) =>
  error.response?.status === 403 &&
  error.response?.data?.message.toLowerCase().includes('api rate limit');

const catchRateLimitError = () =>
  catchError((error) => {
    if (isAxiosError<GithubErrorResponse>(error) && isRateLimitError(error)) {
      return throwError(
        () => new RateLimitReachedError(GitRemoteSources.GITHUB),
      );
    }

    return throwError(() => error);
  });

@Injectable()
export class GithubClient {
  private readonly commonRequestHeaders: AxiosRequestHeaders;

  constructor(
    private readonly httpService: HttpService,

    @InjectPinoLogger(GithubClient.name)
    private readonly logger: PinoLogger,

    @Inject(githubConfig.KEY)
    private readonly config: ConfigType<typeof githubConfig>,
  ) {
    this.commonRequestHeaders = {
      Accept: 'application/vnd.github+json',
      ...(this.config.accessToken
        ? { Authorization: `Bearer ${this.config.accessToken}` }
        : {}),
    };
  }

  getUserRepositories(username: string): Observable<GithubRepository> {
    let currentPage = 1;

    const query = (page: number) =>
      this.getRepositoriesFromApi(
        username,
        page,
        this.config.pageLimits.repositories,
      );

    return from(query(currentPage)).pipe(
      expand((result) =>
        result.length === this.config.pageLimits.repositories
          ? query(++currentPage)
          : EMPTY,
      ),
      mergeMap((repositories) => of(...repositories)),
    );
  }

  getRepositoryBranches(
    repository: GithubRepository,
  ): Observable<GithubRepositoryBranch> {
    let currentPage = 1;

    const query = (page: number) =>
      this.getBranchesFromApi(
        repository.full_name,
        page,
        this.config.pageLimits.branches,
      );

    return from(query(currentPage)).pipe(
      expand((result) =>
        result.length === this.config.pageLimits.branches
          ? query(++currentPage)
          : EMPTY,
      ),
      mergeMap((repositories) => of(...repositories)),
    );
  }

  /**
   * Performs call to [Github API](https://docs.github.com/en/rest/repos/repos#list-repositories-for-a-user)
   * to fetch user repositories
   * @param username github username
   * @param page current page to fetch
   * @param perPage repositories per page
   *
   * @throws {UserNotFoundError} if user was not found in github
   */
  private getRepositoriesFromApi(
    username: string,
    page: number,
    perPage: number,
  ): Observable<GithubRepository[]> {
    this.logger.info(
      `Fetching page #${page} (x${perPage} repos) for user ${username}`,
    );

    return this.httpService
      .get<GithubRepository[]>(
        `${this.config.apiBaseUrl}/users/${username}/repos`,
        {
          params: {
            page,
            per_page: perPage,
          },
          headers: this.commonRequestHeaders,
        },
      )
      .pipe(
        catchRateLimitError(),
        catchError((error) => {
          if (
            isAxiosError<GithubErrorResponse>(error) &&
            isUserNotFoundError(error)
          ) {
            this.logger.info(`User ${username} not found`);
            return throwError(
              () => new UserNotFoundError(GitRemoteSources.GITHUB, username),
            );
          }

          return throwError(() => error);
        }),
        map(({ data }) => data),
      );
  }

  /**
   * Performs call to [Github API](https://docs.github.com/en/rest/branches/branches#list-branches)
   * to fetch repository branches
   * @param repositoryFullName repository full name including owner and full path
   * @param page current page to fetch
   * @param perPage branches per page
   */
  private getBranchesFromApi(
    repositoryFullName: string,
    page: number,
    perPage: number,
  ): Observable<GithubRepositoryBranch[]> {
    this.logger.info(
      `Fetching page #${page} (x${perPage} branches) for repository ${repositoryFullName}`,
    );

    return this.httpService
      .get<GithubRepositoryBranch[]>(
        `${this.config.apiBaseUrl}/repos/${repositoryFullName}/branches`,
        {
          params: {
            page,
            per_page: perPage,
          },
          headers: this.commonRequestHeaders,
        },
      )
      .pipe(
        catchRateLimitError(),
        map(({ data }) => data),
      );
  }
}
