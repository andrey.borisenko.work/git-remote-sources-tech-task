import { deepClone } from '@app/common';
import { of } from 'rxjs';
import { GithubClient } from '../../github.client';
import { githubRepositoryBranchesFixture } from '../fixtures/github-repository-branches.fixture';
import { githubUserRepositoriesFixture } from '../fixtures/github-user-repositories.fixture';

export const githubClientMock: Partial<GithubClient> = {
  getUserRepositories: jest
    .fn()
    .mockImplementation(() => of(...deepClone(githubUserRepositoriesFixture))),

  getRepositoryBranches: jest
    .fn()
    .mockImplementation(() =>
      of(...deepClone(githubRepositoryBranchesFixture)),
    ),
};
