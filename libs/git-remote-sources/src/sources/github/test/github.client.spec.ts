import { HttpModule, HttpService } from '@nestjs/axios';
import { ConfigType } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { AxiosError, AxiosResponse } from 'axios';
import { getLoggerToken } from 'nestjs-pino';
import { of, take, tap, throwError } from 'rxjs';

import { deepClone } from '@app/common';
import { pinoLoggerMock } from '@app/common/test';
import {
  RateLimitReachedError,
  UserNotFoundError,
} from '@app/git-remote-sources/errors';

import { GithubClient } from '../github.client';
import { githubConfig } from '../github.config';
import { githubUserRepositoriesFixture } from './fixtures/github-user-repositories.fixture';
import { githubRepositoryBranchesFixture } from './fixtures/github-repository-branches.fixture';
import { GithubRepository } from '../github.interfaces';

describe('GithubClient', () => {
  let client: GithubClient;
  let httpService: HttpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        GithubClient,
        {
          provide: githubConfig.KEY,
          useValue: {
            pageLimits: {},
          } as Partial<ConfigType<typeof githubConfig>>,
        },
        {
          provide: getLoggerToken(GithubClient.name),
          useValue: pinoLoggerMock,
        },
      ],
    }).compile();

    client = module.get(GithubClient);
    httpService = module.get(HttpService);
  });

  it('should be defined', () => {
    expect(client).toBeDefined();
  });

  it('should get user repositories', (done) => {
    expect.assertions(1);

    jest
      .spyOn(httpService, 'get')
      .mockImplementationOnce(() =>
        of({ data: deepClone(githubUserRepositoriesFixture) } as AxiosResponse),
      );

    const userRepos$ = client.getUserRepositories('test');

    userRepos$.pipe(take(1)).subscribe((repository) => {
      expect(repository).toHaveProperty('name');
      done();
    });
  }, 1000);

  it('should throw UserNotFoundError', (done) => {
    expect.assertions(1);

    jest.spyOn(httpService, 'get').mockImplementationOnce(() =>
      throwError(
        () =>
          new AxiosError('', '404', {}, {}, {
            data: {
              message: 'Not Found',
            },
          } as AxiosResponse),
      ),
    );

    client.getUserRepositories('test').subscribe({
      error: (error) => {
        expect(error).toBeInstanceOf(UserNotFoundError);
        done();
      },
    });
  }, 1000);

  it('should throw RateLimitReachedError', (done) => {
    expect.assertions(1);

    jest.spyOn(httpService, 'get').mockImplementationOnce(() =>
      throwError(
        () =>
          new AxiosError('', '403', {}, {}, {
            status: 403,
            data: {
              message: 'API rate limit exceeded',
            },
          } as AxiosResponse),
      ),
    );

    client.getUserRepositories('test').subscribe({
      error: (error) => {
        expect(error).toBeInstanceOf(RateLimitReachedError);
        done();
      },
    });
  }, 1000);

  it('should get repository branches', (done) => {
    expect.assertions(1);

    jest.spyOn(httpService, 'get').mockImplementationOnce(() =>
      of({
        data: deepClone(githubRepositoryBranchesFixture),
      } as AxiosResponse),
    );

    client
      .getRepositoryBranches({
        full_name: 'test',
      } as GithubRepository)
      .pipe(take(1))
      .subscribe((branch) => {
        expect(branch).toHaveProperty('name');
        done();
      });
  }, 1000);
});
