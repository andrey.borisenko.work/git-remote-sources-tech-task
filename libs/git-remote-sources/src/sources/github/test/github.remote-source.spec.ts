import { Test, TestingModule } from '@nestjs/testing';
import { getLoggerToken } from 'nestjs-pino';

import { GitRemoteSources } from '@app/git-remote-sources/enums';
import {
  RateLimitReachedError,
  UserNotFoundError,
} from '@app/git-remote-sources/errors';
import {
  gitRepositoriesArrayAssertion,
  pinoLoggerMock,
} from '@app/common/test';

import { GithubClient } from '../github.client';
import { GithubRemoteSource } from '../github.remote-source';
import { githubClientMock } from './mocks/github.client.mock';
import { throwError } from 'rxjs';

describe('GithubRemoteSource', () => {
  let remoteSource: GithubRemoteSource;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GithubRemoteSource,
        { provide: GithubClient, useValue: githubClientMock },
        {
          provide: getLoggerToken(GithubRemoteSource.name),
          useValue: pinoLoggerMock,
        },
      ],
    }).compile();

    remoteSource = module.get(GithubRemoteSource);
  });

  it('should be defined', () => {
    expect(remoteSource).toBeDefined();
  });

  it('should get user repositories', async () => {
    const repositories = await remoteSource.getUserOwnRepositories('test');

    gitRepositoriesArrayAssertion(repositories);
  });

  it('should throw UserNotFoundError', async () => {
    jest
      .spyOn(githubClientMock, 'getUserRepositories')
      .mockImplementation(() =>
        throwError(
          () => new UserNotFoundError(GitRemoteSources.GITHUB, 'test'),
        ),
      );

    expect(remoteSource.getUserOwnRepositories('test')).rejects.toThrowError(
      UserNotFoundError,
    );
  });

  it('should throw UserNotFoundError', async () => {
    jest
      .spyOn(githubClientMock, 'getUserRepositories')
      .mockImplementation(() =>
        throwError(() => new RateLimitReachedError(GitRemoteSources.GITHUB)),
      );

    expect(remoteSource.getUserOwnRepositories('test')).rejects.toThrowError(
      RateLimitReachedError,
    );
  });
});
