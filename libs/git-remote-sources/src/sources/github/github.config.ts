import { registerAs } from '@nestjs/config';

export const githubConfig = registerAs('github', () => ({
  pageLimits: {
    repositories: Number(process.env.GITHUB_PAGE_LIMITS_REPOSITORIES) || 100,
    branches: Number(process.env.GITHUB_PAGE_LIMITS_BRANCHES) || 100,
  },
  apiBaseUrl: process.env.GITHUB_API_BASE_URL || 'https://api.github.com',
  /**
   * Use access token to have up to 1000 req/hour rate limit instead of basic 60 req/hour
   */
  accessToken: process.env.GITHUB_ACCESS_TOKEN,
}));
