import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { githubConfig } from './github.config';
import { GithubClient } from './github.client';
import { GithubRemoteSource } from './github.remote-source';

@Module({
  imports: [HttpModule, ConfigModule.forFeature(githubConfig)],
  providers: [GithubClient, GithubRemoteSource],
  exports: [GithubRemoteSource],
})
export class GithubModule {}
