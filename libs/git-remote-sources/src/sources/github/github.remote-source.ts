import { Injectable } from '@nestjs/common';

import { GitRepository } from '@app/domain';

import { GitRemoteSource } from '../../interfaces';
import { GithubClient } from './github.client';
import {
  catchError,
  filter,
  from,
  lastValueFrom,
  map,
  mergeMap,
  of,
  toArray,
} from 'rxjs';
import { InjectPinoLogger, PinoLogger } from 'nestjs-pino';
import { GithubRepositoryBranch } from './github.interfaces';

@Injectable()
export class GithubRemoteSource implements GitRemoteSource {
  constructor(
    private readonly githubClient: GithubClient,

    @InjectPinoLogger(GithubRemoteSource.name)
    private readonly logger: PinoLogger,
  ) {}

  async getUserOwnRepositories(username: string): Promise<GitRepository[]> {
    return await lastValueFrom(
      this.githubClient.getUserRepositories(username).pipe(
        filter((repository) => !repository.fork),
        mergeMap((repository) =>
          this.githubClient.getRepositoryBranches(repository).pipe(
            toArray(),
            catchError((error) => {
              this.logger.error({
                message: `Failed to get branches for ${repository.name}`,
                error,
              });

              return of([] as GithubRepositoryBranch[]);
            }),
            map((branches) => {
              const mappedRepository: GitRepository = {
                name: repository.name,
                owner: repository.owner?.login,
                branches: branches.map((branch) => ({
                  name: branch.name,
                  lastCommitSha: branch.commit?.sha,
                })),
              };

              return mappedRepository;
            }),
          ),
        ),
        toArray(),
      ),
    );
  }
}
