import { GitRepository } from '@app/domain';

export const remoteSourceUserRepositoriesFixture: GitRepository[] = [
  {
    name: 'mocked-1',
    owner: 'mocked',
    branches: [
      {
        name: 'main',
        lastCommitSha: 'f0079c9013f0a35eb029f9671564adc9f21f29a5',
      },
    ],
  },
  {
    name: 'mocked-2',
    owner: 'mocked',
    branches: [
      {
        name: 'main',
        lastCommitSha: 'f0079c9013f0a35eb029f9671564adc9f21f29a5',
      },
    ],
  },
  {
    name: 'mocked-3',
    owner: 'mocked',
    branches: [
      {
        name: 'main',
        lastCommitSha: 'f0079c9013f0a35eb029f9671564adc9f21f29a5',
      },
    ],
  },
];
