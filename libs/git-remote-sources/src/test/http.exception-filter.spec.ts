import { ArgumentsHost, HttpStatus } from '@nestjs/common';
import { AbstractHttpAdapter, HttpAdapterHost } from '@nestjs/core';
import { Test, TestingModule } from '@nestjs/testing';
import { GitRemoteSources } from '../enums';
import {
  RateLimitReachedError,
  UnimplementedRemoteSourceError,
  UserNotFoundError,
} from '../errors';
import { GitRemoteSourcesHttpExceptionsFilter } from '../exception-filters/http.exception-filter';

const argumentHostMock: ArgumentsHost = {
  switchToHttp: jest.fn().mockReturnValue({
    getResponse: jest.fn().mockReturnValue({}),
  }),
} as any as ArgumentsHost;

describe('GitRemoteSourcesHttpExceptionsFilter', () => {
  let filter: GitRemoteSourcesHttpExceptionsFilter;
  let httpAdapterMock: HttpAdapterHost;

  beforeEach(async () => {
    httpAdapterMock = {
      httpAdapter: {
        reply: jest.fn(),
      } as any as AbstractHttpAdapter,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        GitRemoteSourcesHttpExceptionsFilter,
        {
          provide: HttpAdapterHost,
          useValue: httpAdapterMock,
        },
      ],
    }).compile();

    filter = module.get(GitRemoteSourcesHttpExceptionsFilter);
  });

  it('should be defined', () => {
    expect(filter).toBeDefined();
  });

  it('should map UserNotFoundError to 404 response', () => {
    const status = HttpStatus.NOT_FOUND;

    filter.catch(
      new UserNotFoundError(GitRemoteSources.GITHUB, 'test'),
      argumentHostMock,
    );

    expect(httpAdapterMock.httpAdapter.reply).toBeCalledWith(
      expect.anything(),
      expect.objectContaining({ status, Message: expect.any(String) }),
      status,
    );
  });

  it('should map RateLimitReachedError to 429 response', () => {
    const status = HttpStatus.TOO_MANY_REQUESTS;

    filter.catch(
      new RateLimitReachedError(GitRemoteSources.GITHUB),
      argumentHostMock,
    );

    expect(httpAdapterMock.httpAdapter.reply).toBeCalledWith(
      expect.anything(),
      expect.objectContaining({ status, Message: expect.any(String) }),
      status,
    );
  });

  it('should map UnimplementedRemoteSourceError to 500 response', () => {
    const status = HttpStatus.INTERNAL_SERVER_ERROR;

    filter.catch(
      new UnimplementedRemoteSourceError(GitRemoteSources.GITHUB),
      argumentHostMock,
    );

    expect(httpAdapterMock.httpAdapter.reply).toBeCalledWith(
      expect.anything(),
      expect.objectContaining({ status, Message: expect.any(String) }),
      status,
    );
  });
});
