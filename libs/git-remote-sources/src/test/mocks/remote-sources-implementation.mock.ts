import { GitRemoteSources } from '@app/git-remote-sources/enums';
import {
  RemoteSourcesImplementations,
  REMOTE_SOURCES_IMPLEMENTATIONS,
} from '@app/git-remote-sources/remote-sources-implementations.provider';
import { Provider } from '@nestjs/common';
import { remoteSourceMock } from './remote-source.mock';

export const remoteSourcesImplementationsMock: Provider<RemoteSourcesImplementations> =
  {
    provide: REMOTE_SOURCES_IMPLEMENTATIONS,
    useValue: {
      [GitRemoteSources.GITHUB]: remoteSourceMock,
    },
  };
