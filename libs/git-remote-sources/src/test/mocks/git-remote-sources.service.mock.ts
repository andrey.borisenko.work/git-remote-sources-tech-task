import { GitRemoteSources } from '@app/git-remote-sources/enums';
import { GitRemoteSourcesService } from '@app/git-remote-sources/git-remote-sources.service';
import { remoteSourceMock } from './remote-source.mock';

export const gitRemoteSourcesServiceMock: Partial<GitRemoteSourcesService> = {
  getOwnUserRepositories: jest
    .fn()
    .mockImplementation((source: GitRemoteSources, username: string) =>
      remoteSourceMock.getUserOwnRepositories(username),
    ),

  getRemoteSource: jest.fn().mockReturnValue(remoteSourceMock),
};
