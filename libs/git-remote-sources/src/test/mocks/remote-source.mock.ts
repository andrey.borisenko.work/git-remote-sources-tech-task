import { deepClone } from '@app/common';
import { GitRemoteSource } from '@app/git-remote-sources/interfaces';
import { remoteSourceUserRepositoriesFixture } from '../fixtures/remote-source-user-repositories.fixture';

export const remoteSourceMock: GitRemoteSource = {
  getUserOwnRepositories: jest.fn().mockImplementation((username: string) =>
    Promise.resolve(
      deepClone(remoteSourceUserRepositoriesFixture).map((repo) => {
        repo.owner = username;
        return repo;
      }),
    ),
  ),
};
