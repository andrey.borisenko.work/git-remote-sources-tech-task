import { Test, TestingModule } from '@nestjs/testing';
import { GitRemoteSources } from '../enums';
import { UnimplementedRemoteSourceError, UserNotFoundError } from '../errors';

import { GitRemoteSourcesService } from '../git-remote-sources.service';
import { REMOTE_SOURCES_IMPLEMENTATIONS } from '../remote-sources-implementations.provider';
import { remoteSourceMock } from './mocks/remote-source.mock';
import { remoteSourcesImplementationsMock } from './mocks/remote-sources-implementation.mock';

describe('GitRemoteSourcesService', () => {
  describe('remote source implemented', () => {
    let service: GitRemoteSourcesService;

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [GitRemoteSourcesService, remoteSourcesImplementationsMock],
      }).compile();

      service = module.get<GitRemoteSourcesService>(GitRemoteSourcesService);
    });

    it('should be defined', () => {
      expect(service).toBeDefined();
    });

    it('should get remote source implementation', () => {
      const remoteSource = service.getRemoteSource(GitRemoteSources.GITHUB);

      expect(remoteSource).toBeDefined();
      expect(remoteSource).toHaveProperty('getUserOwnRepositories');
    });

    it('should get user own repositories', async () => {
      const repositories = await service.getOwnUserRepositories(
        GitRemoteSources.GITHUB,
        'test',
      );

      expect(repositories).toBeInstanceOf(Array);
      expect(repositories[0].owner).toEqual('test');
    });

    it('should throw UserNotFoundError', async () => {
      jest
        .spyOn(remoteSourceMock, 'getUserOwnRepositories')
        .mockRejectedValueOnce(
          new UserNotFoundError(GitRemoteSources.GITHUB, 'test'),
        );

      await expect(
        service.getOwnUserRepositories(GitRemoteSources.GITHUB, 'test'),
      ).rejects.toThrowError(UserNotFoundError);
    });
  });

  describe('remote source not implemented', () => {
    let service: GitRemoteSourcesService;

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          GitRemoteSourcesService,
          {
            provide: REMOTE_SOURCES_IMPLEMENTATIONS,
            useValue: {},
          },
        ],
      }).compile();

      service = module.get<GitRemoteSourcesService>(GitRemoteSourcesService);
    });

    it('should throw UnimplementedRemoteSourceError', () => {
      expect(() =>
        service.getRemoteSource(GitRemoteSources.GITHUB),
      ).toThrowError(UnimplementedRemoteSourceError);
    });
  });
});
