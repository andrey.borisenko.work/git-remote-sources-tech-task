import { GitRepository } from '@app/domain';

export const gitRepositoriesArrayAssertion = (
  repositories: GitRepository[],
) => {
  expect(repositories).toBeInstanceOf(Array);
  expect(repositories[0]).toHaveProperty('name');
  expect(repositories[0]).toHaveProperty('owner');
  expect(repositories[0]).toHaveProperty('branches');
  expect(repositories[0].branches).toBeInstanceOf(Array);
  expect(repositories[0].branches[0]).toHaveProperty('name');
  expect(repositories[0].branches[0]).toHaveProperty('lastCommitSha');
};
