import { PinoLogger } from 'nestjs-pino';

export const pinoLoggerMock: Partial<PinoLogger> = {
  assign: jest.fn(),
  debug: jest.fn(),
  error: jest.fn(),
  fatal: jest.fn(),
  info: jest.fn(),
  setContext: jest.fn(),
  trace: jest.fn(),
  warn: jest.fn(),
};

Object.defineProperty(pinoLoggerMock, 'logger', {
  get: jest.fn().mockReturnValue({}),
});
