import { AxiosError } from 'axios';
import { isAxiosError } from '../is-axios-error.helper';

describe('Helpers:IsAxiosError', () => {
  it('should return false for non-axios errors', () => {
    const someError = new Error('test');

    expect(isAxiosError(someError)).toBeFalsy();
  });

  it('should narrow type for axios errors', () => {
    const someError = new AxiosError();

    expect(isAxiosError(someError)).toBeTruthy();
  });
});
