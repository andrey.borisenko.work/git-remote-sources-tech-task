import { deepClone } from '../deep-clone.helper';

describe('Helpers:DeepClone', () => {
  it('should clone simple object', () => {
    const original = { a: 1, b: 2 };
    const cloned = deepClone(original);

    expect(original.a === cloned.a).toBeTruthy();
    expect(original !== cloned).toBeTruthy();
  });

  it('should clone nested objects', () => {
    const original = {
      a: 1,
      b: 2,
      nested: {
        value: 'x',
      },
    };
    const cloned = deepClone(original);

    expect(original.a === cloned.a).toBeTruthy();
    expect(original.nested.value === cloned.nested.value).toBeTruthy();
    expect(original !== cloned).toBeTruthy();
    expect(original.nested !== cloned.nested).toBeTruthy();
  });
});
