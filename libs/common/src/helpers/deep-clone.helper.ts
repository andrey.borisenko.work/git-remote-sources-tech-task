/**
 * Cloning objects, arrays with nested structure
 * @param data any data to be cloned
 * @returns same data with different reference in memory to it
 */
export const deepClone = <T>(data: T): T => JSON.parse(JSON.stringify(data));
