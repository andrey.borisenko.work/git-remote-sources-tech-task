import { AxiosError } from 'axios';

/**
 * Narrowing type of error to AxiosError to access fields like request, response, etc.
 * @param error any error to check
 * @returns {boolean}
 */
export const isAxiosError = <T = any>(error: any): error is AxiosError<T> =>
  (error as AxiosError)?.isAxiosError ?? false;
