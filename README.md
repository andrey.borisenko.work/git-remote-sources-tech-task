# Git Remote Sources Tech task

## Table of contents

- [Description](#description)
- [Artifacts](#artifacts)
- [Development](#development)
- [Test](#test)
- [Deployment](#deployment)

## Description

[Nest.js](https://nestjs.com/) application to work with git cloud providers APIs.

Implemented sources:

- Github - [link to API](https://developer.github.com/v3)

## Artifacts

API documented via OpenAPI 3 specification in `swagger/swagger.yaml` file.

Example environment configuration declared in `.env.example`

## Development

```bash
$ npm install

$ npm run start:dev
```

## Test

[`jest`](https://jestjs.io) is used as test-runner

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Deployment

Application ready to be deployed as auto-scaled AWS ECS service (using FARGATE deployment type) fronted by API Gateway.

Infrastructure deployment defined in the
`infrastructure/cloudformation/fargate-api-gw.yaml`
